
</div>
<div class="section">
			<did class="footer-bck py-5">
	<div class="container ">
		<div class="row  mx-4">
			<div class="col-lg-3">
				<ul class="noLst">
				
				<li><a href="<?php
				if( get_locale() == 'nl_NL')
				{echo "/nl/";}else{echo "/";}
				?>" class="footLgo"><img src="/assets/images/EnsembleApp_logo.svg" width="103" alt=""/></a></li>
		
		<li><a href="<?php
				if( get_locale() == 'nl_NL')
				{echo "/nl/";}else{echo "/";}
				?>contact.html" class="footLnk">contact@ensembleapp.com</a></li>
				<li>
				<a href="https://www.linkedin.com/company/ensemblesoftware/" target="_blank" class="smBtn sm-li ml0" title="Ensemble App Linkedin"></a>
					
					<a href="https://www.facebook.com/ensemblesoftware" target="_blank" class="smBtn sm-fb " title="Ensemble App Facebook"></a>
					<a href="https://twitter.com/AppEnsemble" target="_blank" class="smBtn sm-tw" title="Ensemble App Twitter"></a>
					</li>
				
				</ul>
			</div>
			<div class="col-lg-3">
		<?php
				if( get_locale() == 'nl_NL')
	{get_template_part( 'template-parts/ft_nl', get_post_type() );
	 $lng=array('ftCta'=>'Creëer binnen enkele minuten een vlekkeloos evenement',
	'ftBtn'=>'Bekijk een demo',
	'allrights'=>'Alle rechten voorbehouden'

);
	}
	else
	{
	get_template_part( 'template-parts/ft_en', get_post_type() );
		 $lng=array('ftCta'=>'Create Flawless events in minutes',
	'ftBtn'=>'Watch a demo',
	'allrights'=>'All Rights Reserved'

);
	}
				
				
				?>
		<?php wp_nav_menu(array('menu'=>'Languages', 'echo'=>false));
		$lngs=wp_get_nav_menu_items('Languages');
		
	
		
		foreach($lngs as $lang)
		{echo'<a  href="'.$lang->url.'" class="me-1 ms-2" >';
			if($lang->lang=='en-GB'){echo 'English';}else{echo 'Dutch';}
			echo'</a>';}
		
		?>
		
	
		
	 </div>
				
			
			<p class="mt-2"><?php echo $lng['ftCta'];?></p>
			<a href="<?php
				if( get_locale() == 'nl_NL')
				{echo "/nl/";}else{echo "/";}
				?>demo.html" class="btn button-orange button-translate"><?php echo $lng['ftBtn'];?></a>
			</div>
		</div>
	</div>
	</did>
		<div class="copyright">
		<div class="container">
			 <div class="col-12 mx-auto"><p class="text-center my-0 py-3">&copy; Ensemble. <?php echo $lng['allrights'];?> 2020 - 2021</p></div>
			</div>
		</div>
</div>
	

<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/bootstrap.bundle.min.js"></script>
	
	<script type="text/javascript" src="/assets/js/actions.js?v=3"></script>
<?php wp_footer(); ?>

<script>
	
	function checkThis()
		{
			console.log('a');
			var email=$('input[name="frmEm"]').val();
			
			var frmm='Blog subscribe';
			var lng='<?php
				if( get_locale() == 'nl_NL')
				{echo "NL";}else{echo "EN";}
				?>';
			var error=0;
		
			
		
			if(validateEmail(email)==1){$('.err').html('<?php
				if( get_locale() == 'nl_NL')
				{echo "Voer je e-mailadres in a.u.b";}else{echo "Please enter your email address";}
				?>');error+=1;}else{$('.er2').html(' '); }
			
			
			
			if(error==0)
				{
				
					var new_contact = 
			{
			
			'Email': email, //Replace with email of the user
			
			'language':lng,
			'FormSubmited': 'sent',
				'FormType':frmm
		};
					
		var identifier = email;
			
		fwcrm.identify(identifier, new_contact);
		fwcrm.trackCustomEvent("Blog Subscription form",
{
	"email": email
})		;	
<?php 
if( get_locale() == 'nl_NL')
{
?>
$('.response').html(`<div class="py-0"><p class="h3 text-center">Bedankt!</p><p class=" text-center">We hebben je gegevens ontvangen en zullen je vanaf nu op de hoogte houden van onze expert inzichten.<br>Je kunt je op ieder moment uitschrijven.</p></div>`);
<?php
}
else
{?>
$('.response').html(`<div class="py-0"><p class="h3 text-center">Thanks!</p><p class=" text-center">We’ve received your details, and will now keep you up to date with expert insights.<br>You can unsubscribe at any time.</p></div>`);
<?php
}
?>
			
}
		
		}
			
		
		
		
		
			function validateEmail(chkc) {
		   
			var atpos = chkc.indexOf("@");
			var dotpos = chkc.lastIndexOf(".");
			if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=chkc.length)
			 { return 1;}
			 else
			 {return 0;}
		}	
		
</script>
</body>
</html>